import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$x;

public class PasswordPage {
    private final SelenideElement pwInput = $x("//input[@name=\"password\"]");
    public void pwText(String pw){
        pwInput.setValue(pw);
        pwInput.sendKeys(Keys.ENTER);
    }
}
