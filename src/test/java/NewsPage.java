
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;

public class NewsPage {
    private final SelenideElement profile = $x("//li[@id=\"l_pr\"]//a");
    public void jumpToProfile(){
        profile.click();
    }
}
