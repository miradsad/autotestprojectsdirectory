import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$x;

public class AuthPage {
    public AuthPage(String url){
        Selenide.open(url);
    }
    private final SelenideElement loginInput = $x("//input[@class=\"VkIdForm__input\"]");

    public void loginText(String loginString){
        loginInput.setValue(loginString);
        loginInput.sendKeys(Keys.ENTER);
    }
}
