import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class MyTest extends BaseTest {
    private final static String myUrl = "https://vk.com/";
    @Test
    public void checkAuth(){
        AuthPage authPage = new AuthPage(myUrl);
        authPage.loginText(myLogin);
        PasswordPage passwordPage = new PasswordPage();
        passwordPage.pwText(myPassword);
        NewsPage newsPage = new NewsPage();
        newsPage.jumpToProfile();
    }
}
